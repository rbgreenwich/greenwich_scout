<?php

namespace Drupal\greenwich_scout\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class ScoutRoutes implements ContainerInjectionInterface {

	/**
	 * The stream wrapper manager service.
	 *
	 * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
	 */
	protected $streamWrapperManager;

	/**
	 * Constructs a new ImageStyleRoutes object.
	 *
	 * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
	 *	 The stream wrapper manager service.
	 */
	public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager) {
		$this->streamWrapperManager = $stream_wrapper_manager;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function create(ContainerInterface $container) {
		return new static(
			$container->get('stream_wrapper_manager')
		);
	}

	/**
	 * Returns an array of route objects.
	 *
	 * @return \Symfony\Component\Routing\Route[]
	 *	 An array of route objects.
	 */
	public function routes() {
		$routes = [];
		$routes['greenwich_scout.service'] = new Route(
			'/directory/service/{service}',
			[
				'_controller' => 'Drupal\greenwich_scout\Controller\ScoutController::content',
			],
			[
				'_access' => 'TRUE',
			]
		);
		return $routes;
	}
}
