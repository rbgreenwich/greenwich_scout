<?php

namespace Drupal\greenwich_scout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Greenwich Scout Module settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'greenwich_scout_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['greenwich_scout.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['react_app_theme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('Which version of scout are we deploying, generic or other? e.g.')." generic, BFIS, BOD",
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_theme'),
    ];
    $form['react_app_outpost_login_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Outpost login URL'),
      '#description' => $this->t('The URL to where users can sign in to the related outpost install.'),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_outpost_login_url'),
    ];
    $form['react_app_outpost_register_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Outpost register URL'),
      '#description' => $this->t('The URL to where users can register to the related outpost install'),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_outpost_register_url'),
    ];
    $form['react_app_feedback_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Feedback URL'),
      '#description' => $this->t('The URL to a form where users can submit feedback about the service'),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_feedback_url'),
    ];
    $form['react_app_scout_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scout URL'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_scout_url'),
    ];
    $form['react_app_outpost_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Outpost URL'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_outpost_url'),
    ];
    $form['react_app_api_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API host'),
      '#description' => $this->t('The location of the API where service data can be read. e.g. https://example.com/api/v1'),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_api_host'),
    ];
    $form['react_app_filters_datasource'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filters datasource'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_filters_datasource'),
    ];
    $form['react_app_google_client_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google client key'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_google_client_key'),
    ];
    $form['react_app_google_static_maps_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google static maps API key'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_google_static_maps_api_key'),
    ];
    $form['react_app_ga_property_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Analytics property ID'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('react_app_ga_property_id'),
    ];
    $form['notify_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notify API key'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('notify_api_key'),
    ];
    $form['notify_template_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notify template ID'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('notify_template_id'),
    ];
    $form['email_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email host'),
      '#description' => $this->t(''),
      '#default_value' => $this->config('greenwich_scout.settings')->get('email_host'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('greenwich_scout.settings')
      ->set('react_app_theme', $form_state->getValue('react_app_theme'))
      ->set('react_app_outpost_login_url', $form_state->getValue('react_app_outpost_login_url'))
      ->set('react_app_outpost_register_url', $form_state->getValue('react_app_outpost_register_url'))
      ->set('react_app_feedback_url', $form_state->getValue('react_app_feedback_url'))
      ->set('react_app_scout_url', $form_state->getValue('react_app_scout_url'))
      ->set('react_app_outpost_url', $form_state->getValue('react_app_outpost_url'))
      ->set('react_app_api_host', $form_state->getValue('react_app_api_host'))
      ->set('react_app_filters_datasource', $form_state->getValue('react_app_filters_datasource'))
      ->set('react_app_google_client_key', $form_state->getValue('react_app_google_client_key'))
      ->set('react_app_google_static_maps_api_key', $form_state->getValue('react_app_google_static_maps_api_key'))
      ->set('react_app_ga_property_id', $form_state->getValue('react_app_ga_property_id'))
      ->set('notify_api_key', $form_state->getValue('notify_api_key'))
      ->set('notify_template_id', $form_state->getValue('notify_template_id'))
      ->set('email_host', $form_state->getValue('email_host'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
