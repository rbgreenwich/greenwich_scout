<?php
/**
 * @file
 * Contains \Drupal\greenwich_scout\Controller\ScoutController.
 */
namespace Drupal\greenwich_scout\Controller;
class ScoutController {
	public function content() {
		return array(
			'#type' => 'markup',
			'#markup' => "<div id='app'></div>",
			'#attached' => [
				'library' => [
					'greenwich_scout/scout',
				],
			],
		);
	}
}
